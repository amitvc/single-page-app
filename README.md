# Express JS + Angular js app 
This project was put together as a demo application for Single page app using express and angular framework.
The project tries to follow best practices for writing angular controllers, express routes, models and services.

## Installation
1. Download the repository
2. Install npm modules: `npm install`
3. Install bower dependencies `bower install`
4. Start up the server: `node server.js` or 'npm start'
5. View in browser at http://localhost:8080



